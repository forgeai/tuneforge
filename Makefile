.ONESHELL:

SHELL=/bin/bash
CONDA_ACTIVATE=source $$(conda info --base)/etc/profile.d/conda.sh ; conda activate ; conda activate
CONDA_DEACTIVATE=source $$(conda info --base)/etc/profile.d/conda.sh ; conda deactivate ; conda deactivate

.PHONY: install
install:
	conda env create -f environment.yml
	$(CONDA_ACTIVATE) tuneforge
	pre-commit install
	pre-commit autoupdate

.PHONY: uninstall
uninstall:
	$(CONDA_DEACTIVATE)
	conda env remove -n tuneforge

.PHONY: format
format:
	black . -S -l 90

.PHONY: test
test:
	pytest tests

.PHONY: unit
unit:
	TFG_RUN_EAGERLY=True pytest tests/unit

.PHONY: e2e
e2e:
	TFG_RUN_EAGERLY=False pytest tests/e2e

.PHONY: coverage
coverage:
	TFG_RUN_EAGERLY=True coverage run --source core,blacksmiths,scripts -m pytest tests/unit
	coverage html -i
ifeq ($(shell uname -s), Windows_NT)
	start chrome htmlcov/index.html
else
	google-chrome htmlcov/index.html
endif
