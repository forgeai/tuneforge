import logging
from os.path import join

import tensorflow as tf
import tqdm
from munch import Munch
from tensorflow import keras

from blacksmiths.basemith.model import Basemith
from core.blacksmithery.convertors import melspec2audio
from core.blacksmithery.toolset import create_optimizer, Summary, SummaryType
from core.data_pipeline.factory.toolset import get_dataset_size


class Asklepios(Basemith):
    """
    *** ASKLEPIOS represents the God of Medicine ***

    His role in this project is to offer the most basic concrete implementation of
    the abstract basemith. It doesn't have any creative power, but it's good for
    debugging basic functionalities of the pipeline.
    """

    SCOPES = ['training', 'validation']

    def __init__(self, config: Munch, retrain: bool = False):
        super().__init__(config, retrain)
        self.logger = logging.getLogger(__name__)

    def __str__(self) -> str:
        return 'Asklepios'

    def build_model(self) -> None:
        inputs = keras.Input(shape=(None, None, 1))
        x = inputs

        for filters, kernel_size in zip(
            self.config.blacksmith.filters, self.config.blacksmith.kernels
        ):
            x = keras.layers.Conv2D(
                filters=filters,
                kernel_size=kernel_size,
                use_bias=False,
                padding='same',
            )(x)

        outputs = keras.layers.ReLU()(x)
        self.conv_model = keras.Model(inputs=inputs, outputs=outputs)

    def save_model(self) -> None:
        self.conv_model.save(join(self.checkpoints_path, 'conv_model', 'weights.h5'))

    def load_model(self) -> None:
        self.conv_model = keras.models.load_model(
            join(self.checkpoints_path, 'conv_model', 'weights.h5')
        )

    def create_summaries(self, epoch: int, scope: str) -> list:
        """
        Aggregate basic summaries for a given scope (training or validation).

        Args:
            epoch: Iteration number in the training process.
            scope: Partition on which to write the summaries.
        Returns:
            summaries: Contains objects of type Summary.
        """
        if scope == 'training':
            metrics = self.trn_metrics
            samples = self.trn_samples
        else:
            metrics = self.val_metrics
            samples = self.val_samples

        summaries = [
            Summary(
                epoch,
                name,
                SummaryType.SCALAR,
                metrics[name].result(),
            )
            for name in ('mse', 'mae', 'loss')
        ]

        tensor_frequency = self.config.training.summaries.tensor_frequency
        if tensor_frequency > 0 and epoch % tensor_frequency == 0:
            for X in samples:
                M = X['M']
                name = X['name'].numpy()[0].decode('utf-8')

                M_pred = self.conv_model(M, training=False)
                audio_input = melspec2audio(M, **self.config.data.specs)
                audio_pred = melspec2audio(M_pred, **self.config.data.specs)

                summaries += [
                    Summary(
                        epoch,
                        f'audio_input: {name}',
                        SummaryType.AUDIO,
                        audio_input,
                        self.config.data.specs.sr,
                    ),
                    Summary(
                        epoch,
                        f'audio_pred: {name}',
                        SummaryType.AUDIO,
                        audio_pred,
                        self.config.data.specs.sr,
                    ),
                ]

        return summaries

    @tf.function
    def train_step(self, X: tf.Tensor) -> None:
        """
        Perform one iteration of model optimization.

        Args:
            X: A batch containing one audio sample.
        """
        with tf.GradientTape() as tape:
            X_pred = self.conv_model(X, training=True)

            mse = self.loss_fn['mse'](X, X_pred)
            mae = self.loss_fn['mae'](X, X_pred)
            loss = (
                self.config.training.loss_weighting.mse * mse
                + self.config.training.loss_weighting.mae * mae
            )

            trainable_variables = self.conv_model.trainable_variables
            gradients = tape.gradient(loss, trainable_variables)
            self.optimizer.apply_gradients(zip(gradients, trainable_variables))

            self.trn_metrics['mse'](mse)
            self.trn_metrics['mae'](mae)
            self.trn_metrics['loss'](loss)

    @tf.function
    def validation_step(self, X: tf.Tensor) -> None:
        """
        Perform one iteration of model validation.

        Args:
            X: A batch containing one audio sample.
        """
        X_pred = self.conv_model(X, training=False)

        mse = self.loss_fn['mse'](X, X_pred)
        mae = self.loss_fn['mae'](X, X_pred)
        loss = (
            self.config.training.loss_weighting.mse * mse
            + self.config.training.loss_weighting.mae * mae
        )

        self.val_metrics['mse'](mse)
        self.val_metrics['mae'](mae)
        self.val_metrics['loss'](loss)

    def train(self, datasets: dict) -> None:
        trn_dataset = datasets['training']
        val_dataset = datasets['validation']

        self.loss_fn = dict(
            mse=keras.losses.MeanSquaredError(),
            mae=keras.losses.MeanSquaredError(),
        )
        self.trn_metrics = dict(
            mse=keras.metrics.Mean('mse', dtype=tf.float32),
            mae=keras.metrics.Mean('mae', dtype=tf.float32),
            loss=keras.metrics.Mean('loss', dtype=tf.float32),
        )
        self.val_metrics = dict(
            mse=keras.metrics.Mean('mse', dtype=tf.float32),
            mae=keras.metrics.Mean('mae', dtype=tf.float32),
            loss=keras.metrics.Mean('loss', dtype=tf.float32),
        )
        self.optimizer = create_optimizer(
            self.config.training.optimizer, self.config.training.schedule
        )

        starting_epoch = self.epoch.value()
        trn_dataset_size = get_dataset_size(trn_dataset)
        val_dataset_size = get_dataset_size(val_dataset)
        self.trn_samples = trn_dataset.take(3)
        self.val_samples = val_dataset.take(3)

        for epoch in tqdm.trange(
            starting_epoch,
            starting_epoch + self.config.training.epochs,
            desc='Training',
        ):
            self.epoch.assign(epoch)

            for X in tqdm.tqdm(
                trn_dataset, total=trn_dataset_size, desc=f'Epoch {epoch} (trn)'
            ):
                self.train_step(X['M'])
            for X in tqdm.tqdm(
                val_dataset, total=val_dataset_size, desc=f'Epoch {epoch} (val)'
            ):
                self.validation_step(X['M'])

            self.save_model()
            self.save_epoch()

            for scope in Asklepios.SCOPES:
                summaries = self.create_summaries(epoch, scope)
                self.write_summaries(summaries, scope)

            for metric_tensor in self.trn_metrics.values():
                metric_tensor.reset_states()
            for metric_tensor in self.val_metrics.values():
                metric_tensor.reset_states()
