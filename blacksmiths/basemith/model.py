from abc import ABC, abstractmethod
from os.path import join

import tensorflow as tf
from munch import Munch

from blacksmiths.basemith.toolset import (
    check_project_exists,
    create_new_version_paths,
    get_latest_version_paths,
)
from core.blacksmithery.toolset import SummaryType
from core.utils.exceptions import ProjectAlreadyExistsException
from core.utils.json_processing import dict2json


class Basemith(ABC):
    """
    An abstract class serving as template for any blacksmith. It includes implementation
    for some basic operations regarding model creation and loading.

    Args:
        config: Main config object that includes data, blacksmith and training.
        retrain: Whether to continue from a pretrained model if found in the project path.
    """

    def __init__(self, config: Munch, retrain: bool = False):
        self.config = config
        project_exists = check_project_exists(config.blacksmith.project)

        if not project_exists:
            (
                self.model_path,
                self.summaries_path,
                self.checkpoints_path,
            ) = create_new_version_paths(config.blacksmith.project)
            dict2json(config, join(self.model_path, 'config.json'))

            self.build_model()
            self.initialize_epoch()
            self.create_epoch_checkpoint_manager()

        elif retrain:
            (
                self.model_path,
                self.summaries_path,
                self.checkpoints_path,
            ) = get_latest_version_paths(config.blacksmith.project)

            self.build_model()
            self.initialize_epoch()
            self.create_epoch_checkpoint_manager()

            self.load_model()
            self.load_epoch()
            (
                self.model_path,
                self.summaries_path,
                self.checkpoints_path,
            ) = create_new_version_paths(config.blacksmith.project)

            self.create_epoch_checkpoint_manager()
            dict2json(config, join(self.model_path, 'config.json'))

        else:
            message = (
                'Use a different project name or pass the flag --retrain if '
                'you want to resume training using the latest model version.'
            )
            raise ProjectAlreadyExistsException(config.blacksmith.project, message)

    def create_epoch_checkpoint_manager(self) -> None:
        """
        Create the epoch checkpoint manager which is responsible for saving
        and loading the epoch tensor.
        """
        self.epoch_checkpoint = tf.train.Checkpoint(epoch=self.epoch)
        self.epoch_checkpoint_manager = tf.train.CheckpointManager(
            checkpoint=self.epoch_checkpoint,
            directory=join(self.checkpoints_path, 'epoch'),
            max_to_keep=1,
        )

    def initialize_epoch(self) -> None:
        """
        Create the epoch tensor and set it to 0.
        """
        self.epoch = tf.Variable(initial_value=0, trainable=False, dtype=tf.int64)

    def save_epoch(self) -> None:
        """
        Checkpoint the epoch value.
        """
        self.epoch_checkpoint_manager.save()

    def load_epoch(self) -> None:
        """
        Load the epoch value from the checkpoint.
        """
        self.epoch_checkpoint.restore(
            self.epoch_checkpoint_manager.latest_checkpoint
        ).assert_consumed()

    def open_summary_writers(self, scopes: list) -> None:
        """
        Open the writers that store summaries for tensorboard.

        Args:
            scopes: All different summary partitions used in the session.
        """
        self.summary_writer = {
            scope: tf.summary.create_file_writer(join(self.summaries_path, scope))
            for scope in scopes
        }

    def close_summary_writers(self) -> None:
        """
        Close the writers that store summaries for tensorboard.
        """
        for writer in self.summary_writer.values():
            writer.close()

    def write_summaries(self, summaries: list, scope: str) -> None:
        """
        Write a list of tensors to the summary file that corresponds to the `scope`.

        Args:
            summaries: Contains objects of type Summary.
            scope: Partition on which to write the summaries.
        """
        with self.summary_writer[scope].as_default():
            for sm in summaries:
                match sm.type:
                    case SummaryType.SCALAR:
                        tf.summary.scalar(sm.name, sm.tensor, step=sm.step)
                    case SummaryType.IMAGE:
                        tf.summary.image(sm.name, sm.tensor, step=sm.step)
                    case SummaryType.AUDIO:
                        tf.summary.audio(sm.name, sm.tensor, sm.sample_rate, step=sm.step)
                    case _:
                        raise TypeError(f'Cannot summarize {sm.type}.')

    @abstractmethod
    def build_model(self) -> None:
        """
        Build the tensorflow architecture of the model.
        """
        raise NotImplementedError

    @abstractmethod
    def save_model(self) -> None:
        """
        Checkpoint all trainable weights of the model.
        """
        raise NotImplementedError

    @abstractmethod
    def load_model(self) -> None:
        """
        Load all trainable weights of the model.
        """
        raise NotImplementedError

    @abstractmethod
    def train(self, datasets: dict) -> None:
        """
        Perform the model training process.

        Args:
            datasets: Maps dataset name/stream to tf.data.Dataset object.
        """
        raise NotImplementedError
