from os import listdir, makedirs
from os.path import isdir, join
from shutil import copytree


def get_latest_version(project: str) -> int:
    """
    Get the most recent version number of the model.

    Args:
        project: Directory where the project is located.
    Returns:
        The highest version number, or None if no version exists.
    """
    versions = list(filter(lambda x: x.isdigit(), listdir(project)))
    if len(versions):
        return max(map(int, versions))
    return None


def check_project_exists(project: str) -> bool:
    """
    Verify if a project exists, meaning that it has at least a model version.

    Args:
        project: Directory where the project is located.
    Returns:
        True if a model version exists in the project, False otherwise.
    """
    if isdir(project):
        return get_latest_version(project) is not None
    return False


def create_new_version_paths(project: str) -> tuple:
    """
    Compose the paths to all model storage folders for a new version and
    create the directories on disk if they don't already exist.

    Args:
        project: Directory where the project is located.
    Returns:
        model_path: Path to the model/version directory.
        summaries_path: Path to the tensorboard summaries directory.
        checkpoints_path: Path to the serialized checkpoints directory.
    """
    makedirs(project, exist_ok=True)
    latest_version = get_latest_version(project)
    new_version = 1 if latest_version is None else latest_version + 1

    model_path = join(project, str(new_version))
    summaries_path = join(model_path, 'summaries')
    checkpoints_path = join(model_path, 'checkpoints')
    makedirs(summaries_path, exist_ok=False)
    makedirs(checkpoints_path, exist_ok=False)

    return model_path, summaries_path, checkpoints_path


def get_latest_version_paths(project: str) -> tuple:
    """
    Compose the paths to all model storage folders for the latest existing version.

    Args:
        project: Directory where the project is located.
    Returns:
        model_path: Path to the model/version directory.
        summaries_path: Path to the tensorboard summaries directory.
        checkpoints_path: Path to the serialized checkpoints directory.
    """
    latest_version = get_latest_version(project)

    model_path = join(project, str(latest_version))
    summaries_path = join(model_path, 'summaries')
    checkpoints_path = join(model_path, 'checkpoints')

    return model_path, summaries_path, checkpoints_path


def copy_checkpoint_folder(model_path: str, checkpoints_path: str, epoch: int) -> None:
    """
    Create a snapshot of the current checkpoint folder.

    Args:
        model_path: Path to the model/version directory.
        checkpoints_path: Path to the checkpoints directory.
        epoch: Epoch number for the current checkpoint.
    """
    checkpoint_copies = join(model_path, 'checkpoint_storage')
    makedirs(checkpoint_copies, exist_ok=True)
    copytree(checkpoints_path, join(checkpoint_copies, f'checkpoint_{epoch:05d}'))
