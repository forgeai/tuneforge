import logging
from copy import deepcopy
from os import makedirs
from os.path import join

import numpy as np
import tensorflow as tf
import tqdm
from munch import Munch
from tensorflow import keras

from blacksmiths.basemith.model import Basemith
from blacksmiths.britomartis.toolset import group_spec_slices
from core.blacksmithery.components.factory import create_component
from core.blacksmithery.toolset import create_optimizer, Summary, SummaryType
from core.data_pipeline.factory.toolset import get_dataset_size


class Britomartis(Basemith):
    """
    *** BRITOMARTIS is the goddess of mountains and hunting ***

    She was primarily worshipped on the island of Crete, but now she transposed herself
    into a blacksmith that crafts chord progressions using VAEs.
    """

    SCOPES = ['training', 'validation']

    def __init__(self, config: Munch, retrain: bool = False):
        super().__init__(config, retrain)
        self.logger = logging.getLogger(__name__)

    def __str__(self) -> str:
        return 'Britomartis'

    def _get_weights_path(self, component: str) -> str:
        """
        Return the path to the saved weights (.h5 file) for the component.

        Args:
            component: Either 'encoder' or 'decoder'.
        Returns:
            Path to the .h5 file with the saved weights.
        """
        return join(self.checkpoints_path, component, 'weights.h5')

    def build_model(self) -> None:
        encoder_config = deepcopy(self.config.blacksmith.encoder)
        encoder_config.input_shape = (
            self.config.data.specs.n_mels,
            self.config.blacksmith.slice.size,
            1,
        )
        self.encoder = create_component(encoder_config)

        decoder_config = deepcopy(self.config.blacksmith.decoder)
        decoder_config.input_shape = self.encoder.output_shape[1:]
        self.decoder = create_component(decoder_config)

        orig_shape = np.asarray(self.encoder.input_shape)
        reco_shape = np.asarray(self.decoder.output_shape)

        for orig_dim, reco_dim in zip(orig_shape, reco_shape):
            if orig_dim is None or reco_dim is None:
                continue
            if orig_dim != reco_dim:
                raise ValueError(
                    f"Original slice shape and reconstructed slice "
                    f"shape must be compatible but they are "
                    f"{orig_shape} and {reco_shape}."
                )

    def save_model(self) -> None:
        self.encoder.save(self._get_weights_path('encoder'))
        self.decoder.save(self._get_weights_path('decoder'))

    def load_model(self) -> None:
        self.encoder = keras.models.load_model(self._get_weights_path('encoder'))
        self.decoder = keras.models.load_model(self._get_weights_path('decoder'))

    def create_summaries(self, epoch: int, scope: str) -> list:
        """
        Aggregate basic summaries for a given scope (training or validation).

        Args:
            epoch: Iteration number in the training process.
            scope: Partition on which to write the summaries.
        Returns:
            summaries: Contains objects of type Summary.
        """
        if scope == 'training':
            metrics = self.trn_metrics
        else:
            metrics = self.val_metrics

        summaries = [
            Summary(
                epoch,
                name,
                SummaryType.SCALAR,
                metrics[name].result(),
            )
            for name in ('mse',)
        ]

        return summaries

    @tf.function
    def train_step(self, slice_pair: dict) -> None:
        """
        Perform one iteration of model optimization.

        Args:
            slice_pair: Contains 2 spectrogram slices and their corresponding
                indices from the original music piece.
        """
        with tf.GradientTape() as tape:
            encoded_1 = self.encoder(slice_pair['slice_1'], training=True)
            encoded_2 = self.encoder(slice_pair['slice_2'], training=True)
            decoded_1 = self.decoder(encoded_1, training=True)
            decoded_2 = self.decoder(encoded_2, training=True)

            mse_1 = self.loss_fn['mse'](slice_pair['slice_1'], decoded_1)
            mse_2 = self.loss_fn['mse'](slice_pair['slice_2'], decoded_2)
            mse = (mse_1 + mse_2) / 2

            trainable_variables = (
                self.encoder.trainable_variables + self.decoder.trainable_variables
            )
            gradients = tape.gradient(mse, trainable_variables)
            self.optimizer.apply_gradients(zip(gradients, trainable_variables))

            self.trn_metrics['mse'](mse)

    @tf.function
    def validation_step(self, slice_pair: dict) -> None:
        """
        Perform one iteration of model validation.

        Args:
            slice_pair: Contains 2 spectrogram slices and their corresponding
                indices from the original music piece.
        """
        encoded_1 = self.encoder(slice_pair['slice_1'], training=False)
        encoded_2 = self.encoder(slice_pair['slice_2'], training=False)
        decoded_1 = self.decoder(encoded_1, training=False)
        decoded_2 = self.decoder(encoded_2, training=False)

        mse_1 = self.loss_fn['mse'](slice_pair['slice_1'], decoded_1)
        mse_2 = self.loss_fn['mse'](slice_pair['slice_2'], decoded_2)
        mse = (mse_1 + mse_2) / 2

        self.val_metrics['mse'](mse)

    def train(self, datasets: dict) -> None:
        trn_dataset = datasets['training']
        val_dataset = datasets['validation']

        self.loss_fn = dict(
            mse=keras.losses.MeanSquaredError(),
        )
        self.trn_metrics = dict(
            mse=keras.metrics.Mean('mse', dtype=tf.float32),
        )
        self.val_metrics = dict(
            mse=keras.metrics.Mean('mse', dtype=tf.float32),
        )
        self.optimizer = create_optimizer(
            self.config.training.optimizer, self.config.training.schedule
        )

        cache_slices = getattr(self.config.training, 'cache_slices', None)
        if cache_slices is not None and len(cache_slices) > 0:
            makedirs(cache_slices)

        starting_epoch = self.epoch.value()
        trn_dataset_size = get_dataset_size(trn_dataset)
        val_dataset_size = get_dataset_size(val_dataset)
        self.trn_samples = trn_dataset.take(3)
        self.val_samples = val_dataset.take(3)

        for epoch in tqdm.trange(
            starting_epoch,
            starting_epoch + self.config.training.epochs,
            desc='Training',
        ):
            self.epoch.assign(epoch)

            for X in tqdm.tqdm(
                trn_dataset, total=trn_dataset_size, desc=f'Epoch {epoch} (trn)'
            ):
                M = X['M']
                song_name = X['name'].numpy()[0].decode('utf-8')

                slices_dataset = group_spec_slices(self.config.blacksmith.slice, M)
                slices_dataset = slices_dataset.batch(self.config.training.batch_size)
                if cache_slices is not None:
                    filename = "" if cache_slices == "" else join(cache_slices, song_name)
                    slices_dataset = slices_dataset.cache(filename)

                for slice_pair in slices_dataset:
                    self.train_step(slice_pair)

            for X in tqdm.tqdm(
                val_dataset, total=val_dataset_size, desc=f'Epoch {epoch} (val)'
            ):
                M = X['M']
                song_name = X['name'].numpy()[0].decode('utf-8')

                slices_dataset = group_spec_slices(self.config.blacksmith.slice, M)
                slices_dataset = slices_dataset.batch(self.config.training.batch_size)
                if cache_slices is not None:
                    filename = "" if cache_slices == "" else join(cache_slices, song_name)
                    slices_dataset = slices_dataset.cache(filename)

                for slice_pair in slices_dataset:
                    self.validation_step(slice_pair)

            self.save_model()
            self.save_epoch()

            for scope in Britomartis.SCOPES:
                summaries = self.create_summaries(epoch, scope)
                self.write_summaries(summaries, scope)

            for metric_tensor in self.trn_metrics.values():
                metric_tensor.reset_states()
            for metric_tensor in self.val_metrics.values():
                metric_tensor.reset_states()
