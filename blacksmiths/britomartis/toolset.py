import numpy as np
import tensorflow as tf
from munch import Munch


def group_spec_slices(config: Munch, X: tf.Tensor) -> tf.data.Dataset:
    """
    Create a stack of all slices from input spectrogram `X` and pair
    the close ones up. Add slice index information as well.

    Args:
        config: Configurations for patch slicing.
        X: A batch containing the spectrogram for one audio sample.
    Returns:
        A dataset containing a chain of spectrogram slices and the
        start / finish indices for each of them.
    """
    slice_starts = np.arange(0, X.shape[2] - config.size + 1, config.stride)
    slice_ends = slice_starts + config.size
    slices = [X[0, :, i : i + config.size] for i in slice_starts]

    grouping_indices = [
        (i, j)
        for i in range(len(slices) - config.chain)
        for j in range(i + 1, i + config.chain + 1)
    ]
    slices_1 = [slices[i] for i, _ in grouping_indices]
    slices_2 = [slices[j] for _, j in grouping_indices]
    slice_indices_1 = [[slice_starts[i], slice_ends[i]] for i, _ in grouping_indices]
    slice_indices_2 = [[slice_starts[j], slice_ends[j]] for _, j in grouping_indices]

    return tf.data.Dataset.from_tensor_slices(
        dict(
            slice_1=slices_1,
            slice_2=slices_2,
            slice_indices_1=slice_indices_1,
            slice_indices_2=slice_indices_2,
        )
    )
