from munch import Munch
from tensorflow import keras


def create_basic(config: Munch) -> keras.Model:
    """
    Create a feature extraction model based on the specified configs.

    Args:
        config: Contains all the required parameters to create the model.
    Returns:
        Feature extraction keras model object.
    """
    inputs = keras.Input(shape=config.input_shape)
    x = inputs

    for layer_config in config.layers:
        layer_type = layer_config.pop("layer")
        Layer = getattr(keras.layers, layer_type)
        layer = Layer.from_config(layer_config)
        x = layer(x)

    outputs = x
    return keras.Model(inputs=inputs, outputs=outputs)
