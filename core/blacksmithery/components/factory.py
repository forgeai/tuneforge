from munch import Munch
from tensorflow import keras

from core.blacksmithery.components.basic import create_basic


def create_component(config: Munch) -> keras.Model:
    """
    Create a feature extraction (encoder) or slice reconstruction (decoder) model
    based on the specified name and configs.

    Args:
        config: Includes the name of the component and all other required parameters.
    Returns:
        Feature extraction keras model object.
    """
    component_lookup = {'basic': create_basic}
    component_creator = component_lookup[config['type']]
    return component_creator(config)
