import inspect

import librosa
import numpy as np
import tensorflow as tf


def melspec2audio(melspec: tf.Tensor, **kwargs) -> tf.Tensor:
    """
    Convert a melspectrogram tensor to an audio waveform tensor.

    Args:
        melspec: Melsectrogram that can include batch and channel dimensions.
        **kwargs: Arbitrary keyword arguments for reconstructing the audio signal.
    Returns:
        Audio waveform in shape (1, frames, 1).
    """
    m2a_parameters = inspect.signature(librosa.feature.inverse.mel_to_audio).parameters
    m2a_kwargs = {k: v for k, v in kwargs.items() if k in m2a_parameters}

    M = tf.squeeze(melspec).numpy()
    Y = librosa.feature.inverse.mel_to_audio(M=M, **m2a_kwargs)
    Y = Y[np.newaxis, :, np.newaxis]
    return tf.constant(Y)
