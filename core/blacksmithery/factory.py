from munch import Munch

from blacksmiths.asklepios.model import Asklepios
from blacksmiths.britomartis.model import Britomartis
from blacksmiths.basemith.model import Basemith


def create_model(config: Munch, retrain: bool = False) -> Basemith:
    """
    Create a blacksmith model object of the specified type (name).

    Args:
        config: Main config object that includes data, blacksmith and training.
        retrain: Whether to continue from a pretrained model if found in the project path.
    Returns:
        Blacksmith object.
    """
    blacksmiths_lookup = {'asklepios': Asklepios, 'britomartis': Britomartis}
    Blacksmith = blacksmiths_lookup[config.blacksmith.name]
    return Blacksmith(config, retrain)
