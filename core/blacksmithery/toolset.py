from dataclasses import dataclass
from enum import Enum

import tensorflow as tf
from tensorflow import keras


class SummaryType(Enum):
    """
    Enum representing types of tensors to summarize.
    """

    SCALAR = 0
    IMAGE = 1
    AUDIO = 2


@dataclass
class Summary:
    """
    Class representing tensors to summarize for tensorboard.
    """

    step: int
    name: str
    type: SummaryType
    tensor: tf.Tensor
    sample_rate: int = None


def create_optimizer(
    optimizer_config: dict, schedule_config: dict
) -> keras.optimizers.Optimizer:
    """
    Create an optimizer with a custom learning rate schedule.

    Args:
        optimizer_config: Class name and parameters for the optimizer.
        schedule_config: Class name and parameters for the schedule.
    Returns:
        Optimizer object.
    """
    lr_schedule = keras.optimizers.schedules.deserialize(schedule_config)
    return keras.optimizers.deserialize(optimizer_config, learning_rate=lr_schedule)
