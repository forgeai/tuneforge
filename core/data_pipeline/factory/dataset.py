import tensorflow as tf
from munch import Munch

from core.data_pipeline.processing.extract import extract_dataset
from core.data_pipeline.processing.load import load_dataset
from core.data_pipeline.processing.transform import transform_dataset


def create_dataset(config: Munch) -> tf.data.Dataset:
    """
    Create dataset object based on the configurations provided. The dataset is
    loaded from tfrecords stored on disk and goes through the ETL process.

    Args:
        config: Data processing Munch object.
    Returns:
        Dataset object
    """
    dataset = extract_dataset(config.extract)
    dataset = transform_dataset(dataset, config.transform)
    dataset = load_dataset(dataset, config.load)
    return dataset
