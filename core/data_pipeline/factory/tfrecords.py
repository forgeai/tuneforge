from copy import deepcopy
from os import makedirs
from os.path import isfile, join
from shutil import rmtree

from munch import Munch

from core.data_pipeline.storage.tfrecords import serialize_tfrecords
from core.utils.json_processing import dict2json, json2munch


def create_tfrecords(specs: Munch) -> None:
    """
    Create tfrecords based on the configurations provided.
    Save metadata regarding data specs and record specific info.

    Args:
        specs: Data specifications Munch object.
    """
    if specs.type != 'melspecs':
        raise NotImplementedError(f'"{specs.type}" not supported.')

    specs_file = join(specs.records_path, 'specs.json')

    def reset_records_space():
        rmtree(specs.records_path, ignore_errors=True)
        makedirs(specs.records_path)
        dict2json(specs, specs_file)

    if isfile(specs_file):
        cached_specs = json2munch(specs_file)
        if specs != cached_specs:
            reset_records_space()
    else:
        reset_records_space()

    serialize_args = deepcopy(specs)
    del serialize_args.type
    serialize_tfrecords(**serialize_args)
