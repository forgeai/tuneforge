import tensorflow as tf


def get_dataset_size(dataset: tf.data.Dataset) -> int:
    """
    Get the size of the dataset (number of samples).

    Args:
        dataset: Dataset object containing audio samples.
    Returns:
        Total size of the dataset.
    """
    return sum(1 for _ in dataset)
