from importlib import import_module
from os.path import sep

from munch import Munch


def organize_data(config: Munch) -> None:
    """
    Control and delegate data organization functions for processing the
    dataframe file (df.csv) prior to starting the training session.

    Args:
        config: Data organization Munch object.
    """
    for _, org_stage in sorted(config.items(), key=lambda x: int(x[0])):
        module, func_name = org_stage.func.split('::')
        module = module.replace(sep, '.')

        imp = import_module(module)
        func = getattr(imp, func_name)
        func(**org_stage.args)
