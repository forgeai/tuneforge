import random
from os.path import join

import polars as pl


def random_splitter(records_path: str, splits: dict) -> None:
    """
    Perform random splitting of the records according to the split names and
    their corresponding proportions defined in `splits`.

    Args:
        records_path: Path to the directory that will contain the tfrecords.
        splits: Maps split name to data proportion. Values must add up to 1.
    """
    assert all(v > 0 for v in splits.values()), 'Split proportions must be positive'
    assert abs(1.0 - sum(splits.values())) < 1e-06, 'Split proportions must sum up to 1'

    df_file = join(records_path, 'df.csv')
    df = pl.read_csv(df_file)

    total_records = len(df)
    indices = list(range(total_records))
    split_to_indices = {}

    for i, (split_name, split_value) in enumerate(splits.items()):
        if i == len(splits) - 1:
            split_to_indices[split_name] = indices
        else:
            num_records = round(split_value * total_records)
            split_indices = random.sample(indices, num_records)

            split_to_indices[split_name] = split_indices
            indices = list(set(indices) - set(split_indices))

    df_split_list = df['split'].to_numpy()
    for split_name, split_indices in split_to_indices.items():
        df_split_list[split_indices] = split_name

    df.replace('split', pl.Series(df_split_list.tolist(), dtype=df['split'].dtype))
    df.write_csv(df_file)
