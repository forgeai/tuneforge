from os.path import join

import polars as pl
import tensorflow as tf
from munch import Munch

from core.data_pipeline.storage.tfrecords import example2melspec


def extract_dataset(config: Munch) -> tf.data.Dataset:
    """
    Load tfrecords from disk and return a dataset of deserialized samples.
    Use filters to select only records that match the specified constraints.

    Args:
        config: Contains configurations for tfrecords extraction.
    Returns:
        Dataset object.
    """
    df_file = join(config.records_path, 'df.csv')
    df = pl.read_csv(df_file)

    if hasattr(config, 'filters'):
        for key, values in config.filters.items():
            for value in values:
                df = df.filter(pl.col(key) == value)

    record_paths = [
        join(config.records_path, f'{record}.tfrecord') for record in df['record']
    ]
    files = tf.data.Dataset.from_tensor_slices(record_paths)
    dataset = files.interleave(
        tf.data.TFRecordDataset, num_parallel_calls=tf.data.AUTOTUNE
    )
    return dataset.map(map_func=example2melspec, num_parallel_calls=tf.data.AUTOTUNE)
