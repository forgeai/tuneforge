import tensorflow as tf
from munch import Munch


def load_dataset(dataset: tf.data.Dataset, config: Munch) -> tf.data.Dataset:
    """
    Optimize the input pipeline by prefetching data in the CPU in
    order to avoid stalls when the GPU waits to consume batches.

    Args:
        dataset: Contains (augmented) data samples.
        config: Contains configurations for dataset loading.
    Returns:
        Optimized dataset object.
    """
    buffer_size = getattr(config, 'buffer_size', tf.data.AUTOTUNE)
    return dataset.prefetch(buffer_size=buffer_size)
