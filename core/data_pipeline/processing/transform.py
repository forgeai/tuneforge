import tensorflow as tf
from munch import Munch

from core.utils.json_processing import is_defined


def _add_channel_dim(element: dict, keys: list) -> dict:
    """
    Add channel dimension to tensors identified by `keys`.
    Leave the other ones unchanged.

    Args:
        element: Dataset element that maps keys to tensors.
        keys: Identifiers for tensors which will be affected.
    Returns:
        Dataset element with updated tensors.
    """
    new_element = {}

    for key, value in element.items():
        if key in keys:
            new_element[key] = tf.expand_dims(value, axis=-1)
        else:
            new_element[key] = value

    return new_element


def transform_dataset(dataset: tf.data.Dataset, config: Munch) -> tf.data.Dataset:
    """
    Prepare the dataset for the training session. Perform data augmentation
    if necessary.

    Args:
        dataset: Contains deserialized data samples.
        config: Contains configurations for data transformations.
    Returns:
        The transformed dataset object.
    """
    if getattr(config, 'add_batch_dim', False):
        dataset = dataset.batch(batch_size=1)

    keys_to_add_ch_dim = getattr(config, 'add_channel_dim', [])
    if keys_to_add_ch_dim:
        dataset = dataset.map(lambda x: _add_channel_dim(x, keys_to_add_ch_dim))

    if is_defined(config, 'shuffle'):
        dataset = dataset.shuffle(**config.shuffle)

    return dataset
