import glob
import logging
from os.path import basename, isfile, join, splitext

import librosa
import polars as pl
import tensorflow as tf
import tqdm


def _bytes_feature(value):
    """
    Returns a bytes_list from a string / byte.
    """
    if isinstance(value, type(tf.constant(0))):
        value = value.numpy()
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _int64_feature(value):
    """
    Returns an int64_list from a bool / enum / int / uint.
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def melspec2example(audio_path: str, name: str, sr: int, **kwargs) -> tf.train.Example:
    """
    Create a byte sequence with data from an audio file.

    Args:
        audio_path: Path to the audio file.
        name: Identifier for the record, like name of the song.
        sr: Sampling rate for reading the audio.
        **kwargs: Arbitrary keyword arguments.
    Returns:
        Byte encoding of the audio serialized as a string.
    """
    y, sr = librosa.load(audio_path, sr=sr)
    M = librosa.feature.melspectrogram(y=y, sr=sr, **kwargs)

    M_serialized = tf.io.serialize_tensor(M)
    name_serialized = tf.io.serialize_tensor(name)

    feature = {
        'name': _bytes_feature(name_serialized),
        'mels': _int64_feature(M.shape[0]),
        'frames': _int64_feature(M.shape[1]),
        'melspec': _bytes_feature(M_serialized),
    }
    example = tf.train.Example(features=tf.train.Features(feature=feature))

    return example.SerializeToString()


def example2melspec(example_proto: tf.train.Example) -> dict:
    """
    Parse a protobuf to recover the original audio.

    Args:
        example_proto: The example protobuf.
    Returns:
        Dict with the name of the record and the unpacked melspec as tf.Tensors.
    """
    audio_feature_description = {
        'name': tf.io.FixedLenFeature([], tf.string),
        'mels': tf.io.FixedLenFeature([], tf.int64),
        'frames': tf.io.FixedLenFeature([], tf.int64),
        'melspec': tf.io.FixedLenFeature([], tf.string),
    }
    features = tf.io.parse_single_example(example_proto, audio_feature_description)

    name = tf.io.parse_tensor(features['name'], out_type=tf.string)
    flat_melspec = tf.io.parse_tensor(features['melspec'], out_type=tf.float32)
    melspec = tf.reshape(flat_melspec, (features['mels'], features['frames']))

    return dict(name=name, M=melspec)


def serialize_tfrecords(data_path: str, records_path: str, format: str, **kwargs) -> None:
    """
    Serialize the entire audio content of a directory into tfrecords.

    Args:
        data_path: Path to the directory that contains audio files.
        records_path: Path to the directory that will contain the tfrecords.
        format: File format accepted for the audio files (starts with a ".").
        **kwargs: Arbitrary keyword arguments to be passed over to `melspec2example`.
    """
    assert format[0] == '.', f'format must start with a "." but instead is {format}'

    logger = logging.getLogger(__name__)
    audio_paths = glob.glob(join(data_path, f'*{format}'))

    df_file = join(records_path, 'df.csv')
    if isfile(df_file):
        df = pl.read_csv(df_file)
    else:
        df = pl.DataFrame(schema=dict(record=pl.Utf8, split=pl.Utf8))

    for audio_path in tqdm.tqdm(
        audio_paths,
        desc='Serializing tfrecords',
        disable=logger.getEffectiveLevel() > logging.INFO,
    ):
        record_name = splitext(basename(audio_path))[0]
        record_file = f'{record_name}.tfrecord'
        record_path = join(records_path, record_file)
        if isfile(record_path):
            continue

        with tf.io.TFRecordWriter(record_path) as writer:
            tf_example = melspec2example(audio_path, record_name, **kwargs)
            writer.write(tf_example)

        new_row = pl.DataFrame(dict(record=record_name, split='default'))
        df = pl.concat([df, new_row])
        df.write_csv(df_file)
