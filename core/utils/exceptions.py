class ProjectAlreadyExistsException(Exception):
    """
    Exception to be raised when a project with the same name already exists.

    Args:
        project: Directory where the project is located.
    """

    def __init__(self, project, additional_message):
        message = f'Cannot create project "{project}" because it already exists.'
        super().__init__(f'{message} {additional_message}')
