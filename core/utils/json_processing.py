import json

from munch import Munch


def dict2json(dict_: dict, json_filepath: str) -> None:
    """
    Write a python dict to a json file.

    Args:
        dict_: The dict to serialize.
        json_filepath: Path to the json file.
    """
    with open(json_filepath, 'w') as json_fp:
        json.dump(dict_, json_fp, indent=4)


def dict2munch(dict_: dict) -> Munch:
    """
    Convert a python dict to a Munch object.
    This means dict keys can now be accessed as attributes as well.
    Sub dictionaries will be recursively converted.

    Args:
        dict_: The dict to convert.
    Returns:
        Munch object.
    """
    return Munch.fromDict(dict_)


def json2dict(json_filepath: str) -> dict:
    """
    Read a json file as a python dict.

    Args:
        json_filepath: Path to the json file.
    Returns:
        Python dict.
    """
    with open(json_filepath, 'r') as json_fp:
        return json.load(json_fp)


def json2munch(json_filepath: str) -> Munch:
    """
    Read a json file as a Munch object.

    Args:
        json_filepath: Path to the json file.
    Returns:
        Munch object.
    """
    return dict2munch(json2dict(json_filepath))


def Config(**kwargs) -> Munch:
    """
    Shortcut function for creating a config object from keyword arguments.

    Args:
        **kwargs: Arbitrary keyword arguments.

    Returns:
        Munch object.
    """
    return dict2munch(dict(**kwargs))


def is_defined(config: Munch, attribute: str) -> bool:
    """
    Check if `config` has the requested `attribute` and it's not null.

    Args:
        config: Munch object to check.
        attribute: Name of the attribute to search for in the `config`.
    Returns:
        True if `config.attribute` is defined and not null.
    """
    return getattr(config, attribute, None) is not None
