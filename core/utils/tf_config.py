import tensorflow as tf
from decouple import config as env_config


def set_tf_environment(**kwargs) -> None:
    """
    Configure TensorFlow low level behaviour prior to starting the session.

    Args:
        **kwargs: Arbitrary keyword arguments for TensorFlow settings.
    """
    try:
        jit_compile = kwargs['jit_compile']
    except KeyError:
        jit_compile = env_config('TFG_JIT_COMPILE', default=False, cast=bool)
    finally:
        if jit_compile:
            tf.config.optimizer.set_jit('autoclustering')

    try:
        run_eagerly = kwargs['run_eagerly']
    except KeyError:
        run_eagerly = env_config('TFG_RUN_EAGERLY', default=False, cast=bool)
    finally:
        tf.config.run_functions_eagerly(run_eagerly)

    try:
        gpu_memory_growth = kwargs['gpu_memory_growth']
    except KeyError:
        gpu_memory_growth = env_config('TFG_GPU_MEMORY_GROWTH', default=True, cast=bool)
    finally:
        gpus = tf.config.list_physical_devices('GPU')
        if gpus:
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, gpu_memory_growth)
