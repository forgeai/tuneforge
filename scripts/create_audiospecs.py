import argparse
import logging
import logging.config
import math
import sys
from functools import partial
from multiprocessing import get_context

from os import makedirs
from os.path import basename, join, splitext

sys.path.append('.')
logging.config.fileConfig('logging.conf')

import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
from moviepy.editor import AudioFileClip, CompositeAudioClip, VideoClip
from moviepy.video.io.bindings import mplfig_to_npimage

from core.utils.json_processing import json2munch

figure, ax = plt.subplots(figsize=(24, 8))


def _compute_alpha_matrix(shape, u, sigma):
    """
    Compute alpha channel for the highlighted part of the spectrogram.
    """
    index_matrix = np.arange(stop=shape[1], dtype=np.float32)
    numerator = np.exp(-((index_matrix - u) ** 2) / (2 * sigma**2))
    denominator = sigma * np.sqrt(2 * np.pi)

    alpha_matrix = numerator / denominator
    alpha_matrix /= alpha_matrix.max()
    return np.broadcast_to(alpha_matrix, shape)


def _compute_frame(t, audio_name, log_M, config, samples_per_second):
    """
    Create a snapshot of the spectrogram at a given time instance.
    """
    s = t * samples_per_second
    alpha = _compute_alpha_matrix(log_M.shape, s, config.highlight_sigma)
    alpha_negative = 1 - alpha

    ax.clear()
    ax.set_title(audio_name)
    librosa.display.specshow(
        log_M,
        y_axis='mel',
        x_axis='time',
        sr=config.melspec.sr,
        hop_length=config.melspec.hop_length,
        alpha=alpha_negative,
        cmap=config.base_cmap,
    )
    librosa.display.specshow(
        log_M,
        y_axis='mel',
        x_axis='time',
        sr=config.melspec.sr,
        hop_length=config.melspec.hop_length,
        alpha=alpha,
        cmap=config.highlight_cmap,
    )
    return t, mplfig_to_npimage(figure)


def _precompute_frames(t_values, audio_name, log_M, config, samples_per_second):
    compute_frame_partial = partial(
        _compute_frame,
        audio_name=audio_name,
        log_M=log_M,
        config=config,
        samples_per_second=samples_per_second,
    )
    frames = {}

    with get_context('spawn').Pool(processes=config.processes) as pool:
        for t, frame in pool.imap_unordered(compute_frame_partial, t_values):
            frames[round(t, 8)] = frame

    return frames


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Process configurations for creating audiospecs. Audiospecs are '
        'spectrograms on which the current timestamp is emphasized while the '
        'corresponding track is played in the background. They are stored in mp4 format.'
    )
    parser.add_argument('-c', '--config-file', help='Path to the config json file.')
    parser.add_argument(
        '-i',
        '--inputs',
        nargs='*',
        default=None,
        help='Audio input filepaths.',
    )
    parser.add_argument(
        '-o',
        '--output',
        default=None,
        help='Location where audiospecs will be saved.',
    )
    args = parser.parse_args()
    return args


def main(args):
    """
    Create audiospecs for the audio input files and save them to disk.
    """
    logger = logging.getLogger(__name__)
    config = json2munch(args.config_file)

    if args.inputs:
        config.inputs = args.inputs
    if args.output:
        config.output = args.output

    if not isinstance(config.inputs, list):
        config.inputs = [config.inputs]
    makedirs(config.output, exist_ok=True)

    for audio_path in config.inputs:
        audio_name = splitext(basename(audio_path))[0]
        video_path = join(config.output, f'{audio_name}.mp4')

        if hasattr(config, 'slice'):
            start_time = config.slice[0]
            end_time = config.slice[1]
            duration = None if end_time is None else end_time - start_time
        else:
            start_time = 0
            end_time = None
            duration = None

        y, _ = librosa.load(
            audio_path, sr=config.melspec.sr, offset=start_time, duration=duration
        )
        M = librosa.feature.melspectrogram(y=y, **config.melspec)
        log_M = librosa.power_to_db(M)

        num_seconds = len(y) / config.melspec.sr
        samples_per_second = log_M.shape[1] / num_seconds
        num_t_values = math.ceil(num_seconds * config.fps)
        t_values = [x / config.fps for x in range(num_t_values)]

        logger.info(f'Processing "{audio_name}": {num_t_values} frames')
        frames = _precompute_frames(
            t_values, audio_name, log_M, config, samples_per_second
        )
        make_frame = lambda t: frames[round(t, 8)]

        audioclip = AudioFileClip(audio_path).subclip(start_time, end_time)
        videoclip = VideoClip(make_frame, duration=num_seconds)
        videoclip.audio = CompositeAudioClip([audioclip])
        videoclip.write_videofile(video_path, fps=config.fps, threads=config.threads)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
