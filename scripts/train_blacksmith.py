import argparse
import logging
import logging.config
import sys

sys.path.append('.')
logging.config.fileConfig('logging.conf')

import tensorflow as tf

from core.blacksmithery.factory import create_model
from core.data_pipeline.factory.dataset import create_dataset
from core.data_pipeline.factory.tfrecords import create_tfrecords
from core.data_pipeline.org.center import organize_data
from core.utils.json_processing import json2munch
from core.utils.tf_config import set_tf_environment


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Process configurations for training blacksmith.'
    )
    parser.add_argument('-c', '--config-file', help='Path to the config json file.')
    parser.add_argument(
        '-r',
        '--retrain',
        action='store_true',
        default=False,
        help='Whether to continue from a pretrained model if found in the project path.',
    )
    args = parser.parse_args()
    return args


def main(args):
    """
    Create and organize the required data if needed and train a new blacksmith.
    """
    config = json2munch(args.config_file)

    # Records serialization
    create_tfrecords(config.data.specs)

    # Samples organization
    organize_data(config.data.org)

    # Datasets building
    datasets = {
        stream: create_dataset(stream_config)
        for stream, stream_config in config.data.processing.items()
    }

    # Blacksmith creation
    model = create_model(config, args.retrain)

    # Training loop
    model.open_summary_writers(model.SCOPES)
    try:
        model.train(datasets)
    finally:
        model.close_summary_writers()


if __name__ == '__main__':
    set_tf_environment()
    tf.keras.backend.clear_session()

    args = parse_arguments()
    main(args)
