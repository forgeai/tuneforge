from os.path import join
from shutil import copytree

import pytest
import tensorflow as tf
from munch import Munch

from core.utils.tf_config import set_tf_environment


def pytest_configure(config):
    set_tf_environment()
    tf.keras.backend.clear_session()


def pytest_collection_modifyitems(items):
    core_identifier = join('tests', 'unit', 'core')
    blacksmiths_identifier = join('tests', 'unit', 'blacksmiths')
    scripts_identifier = join('tests', 'unit', 'scripts')
    e2e_identifier = join('tests', 'e2e')

    for item in items:
        item_path = str(item.fspath)

        if core_identifier in item_path:
            item.add_marker(pytest.mark.run(order=0))
        elif blacksmiths_identifier in item_path:
            item.add_marker(pytest.mark.run(order=1))
        elif scripts_identifier in item_path:
            item.add_marker(pytest.mark.run(order=2))
        elif e2e_identifier in item_path:
            item.add_marker(pytest.mark.run(order=3))


@pytest.fixture(scope='session')
def data_path():
    yield join('tests', 'data')


@pytest.fixture(scope='session')
def audio_wav(data_path):
    yield join(data_path, 'Adrian von Ziegler - Breath of the Forest - trimmed.wav')


@pytest.fixture(scope='session')
def audio_mp3(data_path):
    yield join(data_path, 'Chopin - Nocturne Op9 No1 - trimmed.mp3')


@pytest.fixture(scope='session')
def all_records_path():
    yield join('tests', 'records')


@pytest.fixture(scope='function')
def wav_melspec_records_path(all_records_path):
    yield join(all_records_path, 'melspecs', 'wav_records')


@pytest.fixture(scope='function')
def mp3_melspec_records_path(all_records_path):
    yield join(all_records_path, 'melspecs', 'mp3_records')


@pytest.fixture(scope='session')
def asklepios_project():
    yield join('tests', 'projects', 'asklepios')


@pytest.fixture(scope='function')
def asklepios_project_env(tmpdir, asklepios_project):
    copytree(asklepios_project, join(tmpdir, 'asklepios'))
    yield tmpdir


@pytest.fixture(scope='function')
def basic_asklepios_config():
    yield Munch.fromDict(
        {
            'blacksmith': {
                'name': 'asklepios',
                'project': None,
                'filters': [4, 1],
                'kernels': [3, 1],
            }
        }
    )


@pytest.fixture(scope='function')
def basic_component_config():
    yield Munch.fromDict(
        {
            'type': 'basic',
            'input_shape': [20, 30, 1],
            'layers': [
                {
                    'layer': 'Conv2D',
                    'filters': 8,
                    'kernel_size': 3,
                    'padding': 'same',
                },
                {'layer': 'MaxPool2D', 'pool_size': 2, 'padding': 'same'},
                {'layer': 'ReLU'},
            ],
        }
    )
