import glob
import logging
import subprocess
from os.path import join

import pytest
from munch import Munch

from core.utils.json_processing import dict2json


@pytest.fixture(scope='module')
def train_britomartis_config():
    yield Munch.fromDict(
        {
            'data': {
                'specs': {
                    'type': 'melspecs',
                    'format': None,
                    'data_path': None,
                    'records_path': None,
                    'sr': 44100,
                    'n_fft': 2048,
                    'hop_length': 512,
                    'n_mels': 128,
                },
                'org': {
                    '1': {
                        'func': 'core/data_pipeline/org/splitter::random_splitter',
                        'args': {
                            'records_path': None,
                            'splits': {'training': 0.75, 'validation': 0.25},
                        },
                    }
                },
                'processing': {
                    'training': {
                        'extract': {
                            'records_path': None,
                            'filters': {'split': ['training']},
                        },
                        'transform': {'add_batch_dim': True, 'add_channel_dim': ['M']},
                        'load': {},
                    },
                    'validation': {
                        'extract': {
                            'records_path': None,
                            'filters': {'split': ['validation']},
                        },
                        'transform': {'add_batch_dim': True, 'add_channel_dim': ['M']},
                        'load': {},
                    },
                },
            },
            'blacksmith': {
                'name': 'britomartis',
                'project': None,
                'slice': {'size': 64, 'stride': 16, 'chain': 4},
                'encoder': {
                    'type': 'basic',
                    'layers': [
                        {
                            'layer': 'Conv2D',
                            'filters': 16,
                            'kernel_size': 3,
                            'padding': 'same',
                            'activation': 'selu',
                        },
                        {'layer': 'AveragePooling2D', 'pool_size': 2, 'padding': 'same'},
                        {
                            'layer': 'Conv2D',
                            'filters': 32,
                            'kernel_size': 3,
                            'padding': 'same',
                            'activation': 'selu',
                        },
                    ],
                },
                'decoder': {
                    'type': 'basic',
                    'layers': [
                        {
                            'layer': 'Conv2D',
                            'filters': 16,
                            'kernel_size': 3,
                            'padding': 'same',
                            'activation': 'selu',
                        },
                        {'layer': 'UpSampling2D', 'size': 2, 'interpolation': 'bilinear'},
                        {
                            'layer': 'Conv2D',
                            'filters': 1,
                            'kernel_size': 3,
                            'padding': 'same',
                        },
                    ],
                },
            },
            'training': {
                'epochs': 25,
                'batch_size': 16,
                'optimizer': {'class_name': 'Adam', 'config': {'epsilon': 1e-07}},
                'schedule': {
                    'class_name': 'PolynomialDecay',
                    'config': {
                        'initial_learning_rate': 1e-03,
                        'end_learning_rate': 8e-04,
                        'decay_steps': 25,
                        'power': 1.0,
                    },
                },
                'cache_slices': None,
                'summaries': {'tensor_frequency': 10},
            },
        }
    )


def test_train_britomartis(tmpdir, data_path, train_britomartis_config):
    logger = logging.getLogger(__name__)

    project = join(str(tmpdir), 'britomartis')
    records_path = join(str(tmpdir), 'records')
    script = join('scripts', 'train_blacksmith.py')
    config_path = join(str(tmpdir), 'config.json')
    cache_path = join(str(tmpdir), 'cache')

    train_britomartis_config.blacksmith.project = project
    train_britomartis_config.data.specs.data_path = data_path
    train_britomartis_config.data.specs.records_path = records_path
    train_britomartis_config.data.org['1'].args.records_path = records_path
    train_britomartis_config.data.processing.training.extract.records_path = records_path
    train_britomartis_config.data.processing.validation.extract.records_path = (
        records_path
    )

    def run_command_and_assert_results(command, version_path):
        try:
            subprocess.run(
                command,
                text=True,
                check=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
            )
        except subprocess.CalledProcessError as e:
            logger.error(e.stdout)
            raise
        else:
            ckpt_path = join(version_path, 'checkpoints')
            summ_path = join(version_path, 'summaries')
            summ_pattern = 'events.out.tfevents.*'

            assert len(glob.glob(join(version_path, 'config.json'))) == 1
            assert len(glob.glob(join(ckpt_path, 'encoder', 'weights.h5'))) == 1
            assert len(glob.glob(join(ckpt_path, 'decoder', 'weights.h5'))) == 1
            assert len(glob.glob(join(ckpt_path, 'epoch', 'checkpoint'))) == 1
            assert len(glob.glob(join(ckpt_path, 'epoch', 'ckpt-*.data-*-of-*'))) == 1
            assert len(glob.glob(join(ckpt_path, 'epoch', 'ckpt-*.index'))) == 1
            assert len(glob.glob(join(summ_path, 'training', summ_pattern))) == 1
            assert len(glob.glob(join(summ_path, 'validation', summ_pattern))) == 1

    # Round 1  - train with .wav files
    train_britomartis_config.data.specs.format = '.wav'
    dict2json(train_britomartis_config, config_path)

    command = ['python', script, '--config-file', config_path]
    version_path = join(project, '1')
    run_command_and_assert_results(command, version_path)

    # Round 2 - finetune with .mp3 files and cache slices
    train_britomartis_config.data.specs.format = '.mp3'
    train_britomartis_config.training.cache_slices = cache_path
    dict2json(train_britomartis_config, config_path)

    command += ['--retrain']
    version_path = join(project, '2')
    run_command_and_assert_results(command, version_path)
