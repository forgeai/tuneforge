from os.path import isdir, join

import pytest

from blacksmiths.asklepios.model import Asklepios
from core.utils.exceptions import ProjectAlreadyExistsException


def test_askleipos_to_train(asklepios_project_env, basic_asklepios_config):
    # Project already exists
    project = join(str(asklepios_project_env), 'asklepios')
    basic_asklepios_config.blacksmith.project = project
    with pytest.raises(ProjectAlreadyExistsException):
        Asklepios(basic_asklepios_config)

    # Project doesn't exist
    project = join(str(asklepios_project_env), 'aristaeus')
    basic_asklepios_config.blacksmith.project = project
    model = Asklepios(basic_asklepios_config)
    model_path = join(project, '1')

    assert str(model) == 'Asklepios'
    assert isdir(model_path)


def test_askleipos_to_retrain(asklepios_project_env, basic_asklepios_config):
    # Project already exists
    project = join(str(asklepios_project_env), 'asklepios')
    basic_asklepios_config.blacksmith.project = project
    model = Asklepios(basic_asklepios_config, retrain=True)
    model_path = join(project, '2')

    assert str(model) == 'Asklepios'
    assert isdir(model_path)

    # Project doesn't exist
    project = join(str(asklepios_project_env), 'aristaeus')
    basic_asklepios_config.blacksmith.project = project
    model = Asklepios(basic_asklepios_config, retrain=True)
    model_path = join(project, '1')

    assert str(model) == 'Asklepios'
    assert isdir(model_path)
