from os.path import join
from shutil import copytree

import pytest
from munch import Munch


@pytest.fixture(scope='session')
def britomartis_project():
    yield join('tests', 'projects', 'britomartis')


@pytest.fixture(scope='function')
def britomartis_project_env(tmpdir, britomartis_project):
    copytree(britomartis_project, join(tmpdir, 'britomartis'))
    yield tmpdir


@pytest.fixture(scope='function')
def basic_britomartis_config():
    yield Munch.fromDict(
        {
            'data': {
                'specs': {
                    'n_mels': 128,
                },
            },
            'blacksmith': {
                'name': 'britomartis',
                'project': None,
                'slice': {'size': 64, 'stride': 16, 'chain': 4},
                'encoder': {
                    'type': 'basic',
                    'layers': [
                        {
                            'layer': 'Conv2D',
                            'filters': 16,
                            'kernel_size': 3,
                            'padding': 'same',
                        }
                    ],
                },
                'decoder': {
                    'type': 'basic',
                    'layers': [
                        {
                            'layer': 'Conv2D',
                            'filters': 1,
                            'kernel_size': 3,
                            'padding': 'same',
                        }
                    ],
                },
            },
        }
    )
