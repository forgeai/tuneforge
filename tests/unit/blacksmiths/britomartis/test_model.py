from os.path import isdir, join

import pytest

from blacksmiths.britomartis.model import Britomartis
from core.utils.exceptions import ProjectAlreadyExistsException


def test_britomartis_to_train(britomartis_project_env, basic_britomartis_config):
    # Project already exists
    project = join(str(britomartis_project_env), 'britomartis')
    basic_britomartis_config.blacksmith.project = project
    with pytest.raises(ProjectAlreadyExistsException):
        Britomartis(basic_britomartis_config)

    # Project doesn't exist
    project = join(str(britomartis_project_env), 'diktynna')
    basic_britomartis_config.blacksmith.project = project
    model = Britomartis(basic_britomartis_config)
    model_path = join(project, '1')

    assert str(model) == 'Britomartis'
    assert isdir(model_path)


def test_britomartis_to_retrain(britomartis_project_env, basic_britomartis_config):
    # Project already exists
    project = join(str(britomartis_project_env), 'britomartis')
    basic_britomartis_config.blacksmith.project = project
    model = Britomartis(basic_britomartis_config, retrain=True)
    model_path = join(project, '2')

    assert str(model) == 'Britomartis'
    assert isdir(model_path)

    # Project doesn't exist
    project = join(str(britomartis_project_env), 'diktynna')
    basic_britomartis_config.blacksmith.project = project
    model = Britomartis(basic_britomartis_config, retrain=True)
    model_path = join(project, '1')

    assert str(model) == 'Britomartis'
    assert isdir(model_path)
