import pytest
import tensorflow as tf

from blacksmiths.britomartis.toolset import group_spec_slices
from core.utils.json_processing import Config


@pytest.mark.parametrize('size', [64, 160])
@pytest.mark.parametrize('stride', [16, 17])
@pytest.mark.parametrize('chain', [1, 2, 3])
def test_group_spec_slices(size, stride, chain):
    config = Config(size=size, stride=stride, chain=chain)
    X = tf.random.normal((1, 128, 800, 1))
    slices_dataset = group_spec_slices(config, X)

    dataset_size = 0
    for element in slices_dataset:
        assert element['slice_1'].shape == (128, size, 1)
        assert element['slice_2'].shape == (128, size, 1)
        assert element['slice_1'].dtype == tf.float32
        assert element['slice_2'].dtype == tf.float32
        assert element['slice_indices_1'].shape == (2,)
        assert element['slice_indices_2'].shape == (2,)
        assert element['slice_indices_1'].dtype == tf.int32
        assert element['slice_indices_2'].dtype == tf.int32
        dataset_size += 1

    expected_dataset_size = ((800 - size - stride * chain) // stride + 1) * chain
    assert dataset_size == expected_dataset_size
