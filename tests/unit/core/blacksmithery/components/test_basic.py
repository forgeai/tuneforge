import numpy as np

from core.blacksmithery.components.basic import create_basic


def test_create_component(basic_component_config):
    comp = create_basic(basic_component_config)
    inputs = np.random.random((4, 20, 30, 1))
    outputs = comp(inputs).numpy()

    assert outputs.shape == (4, 10, 15, 8)
    assert np.greater_equal(outputs, 0).all()
