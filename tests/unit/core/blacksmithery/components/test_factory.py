from tensorflow import keras

from core.blacksmithery.components.factory import create_component


def test_create_component(basic_component_config):
    comp = create_component(basic_component_config)
    assert isinstance(comp, keras.Model)
