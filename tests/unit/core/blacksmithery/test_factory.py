import filecmp
from os.path import join

from core.blacksmithery.factory import create_model


def test_create_model(tmpdir, asklepios_project, basic_asklepios_config):
    basic_asklepios_config.blacksmith.project = str(tmpdir)

    model = create_model(basic_asklepios_config)
    assert str(model) == 'Asklepios'

    expected_version = join(str(tmpdir), '1')
    obtained_version = join(asklepios_project, '1')
    comparison = filecmp.dircmp(obtained_version, expected_version)
    assert set(comparison.common) == {'config.json', 'checkpoints', 'summaries'}
