import pytest

from core.data_pipeline.factory.dataset import create_dataset
from core.data_pipeline.factory.toolset import get_dataset_size
from core.utils.json_processing import Config


@pytest.fixture(scope='function')
def data_processing_config():
    yield Config(
        extract=Config(records_path=None),
        transform=Config(
            shuffle=Config(
                buffer_size=2,
                seed=7875,
            ),
            add_batch_dim=True,
        ),
        load=Config(),
    )


@pytest.mark.parametrize(
    'records_path',
    [
        pytest.lazy_fixture('wav_melspec_records_path'),
        pytest.lazy_fixture('mp3_melspec_records_path'),
    ],
)
def test_create_dataset(data_processing_config, records_path):
    data_processing_config.extract.records_path = records_path
    dataset = create_dataset(data_processing_config)

    assert get_dataset_size(dataset) == 4
