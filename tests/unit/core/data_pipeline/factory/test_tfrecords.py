import filecmp
from os.path import join

import polars as pl
import pytest

from core.data_pipeline.factory.tfrecords import create_tfrecords
from core.utils.json_processing import Config, json2munch


@pytest.fixture(scope='function')
def specs(data_path):
    yield Config(
        type='melspecs',
        format=None,
        data_path=data_path,
        records_path=None,
        sr=44100,
        n_fft=2048,
        hop_length=512,
    )


@pytest.mark.parametrize(
    'format, records_path',
    [
        ('.wav', pytest.lazy_fixture('wav_melspec_records_path')),
        ('.mp3', pytest.lazy_fixture('mp3_melspec_records_path')),
    ],
)
def test_create_tfrecords(tmpdir, specs, format, records_path):
    specs.records_path = join(tmpdir, 'new_directory')
    specs.format = format

    create_tfrecords(specs)

    comparison = filecmp.dircmp(specs.records_path, records_path)
    assert len(comparison.common) == 6

    cached_specs = json2munch(join(specs.records_path, 'specs.json'))
    assert cached_specs == specs

    df = pl.read_csv(join(specs.records_path, 'df.csv'))
    assert len(df) == 4
    assert df.columns == ['record', 'split']


def test_create_tfrecords_failure(specs):
    specs.type = 'snails'

    with pytest.raises(NotImplementedError):
        create_tfrecords(specs)
