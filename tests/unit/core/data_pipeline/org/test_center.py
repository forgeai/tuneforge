from os.path import join
from shutil import copytree

import polars as pl
import pytest
from munch import Munch

from core.data_pipeline.org.center import organize_data


@pytest.fixture(scope='module')
def org_config():
    yield Munch.fromDict(
        {
            '1': {
                'func': 'core/data_pipeline/org/splitter::random_splitter',
                'args': {
                    'records_path': None,
                    'splits': {'training': 0.75, 'validation': 0.25},
                },
            }
        }
    )


@pytest.mark.parametrize(
    'records_path',
    [
        pytest.lazy_fixture('wav_melspec_records_path'),
        pytest.lazy_fixture('mp3_melspec_records_path'),
    ],
)
def test_organize_data(tmpdir, records_path, org_config):
    records_copy_path = join(tmpdir, 'records_path')
    org_config['1'].args.records_path = records_copy_path
    copytree(records_path, records_copy_path)

    df_file = join(records_copy_path, 'df.csv')
    df_before = pl.read_csv(df_file)
    assert df_before['split'].unique().to_list() == ['default']

    organize_data(org_config)
    df_after = pl.read_csv(df_file)
    value_counts = df_after['split'].value_counts().to_dict(as_series=False)
    obtained = {k: v for k, v in zip(value_counts['split'], value_counts['counts'])}
    expected = dict(training=3, validation=1)
    assert obtained == expected
