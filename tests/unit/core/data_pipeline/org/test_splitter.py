from os.path import join
from shutil import copytree

import polars as pl
import pytest

from core.data_pipeline.org.splitter import random_splitter


@pytest.mark.parametrize(
    'records_path',
    [
        pytest.lazy_fixture('wav_melspec_records_path'),
        pytest.lazy_fixture('mp3_melspec_records_path'),
    ],
)
@pytest.mark.parametrize(
    'splits, expected',
    [
        (dict(a=1.0), dict(a=4)),
        (dict(a=0.75, b=0.25), dict(a=3, b=1)),
        (dict(a=0.56, b=0.23, c=0.21), dict(a=2, b=1, c=1)),
    ],
)
def test_random_splitter(tmpdir, records_path, splits, expected):
    records_copy_path = join(tmpdir, 'records_path')
    copytree(records_path, records_copy_path)

    df_file = join(records_copy_path, 'df.csv')
    df_before = pl.read_csv(df_file)
    assert df_before['split'].unique().to_list() == ['default']

    random_splitter(records_copy_path, splits)
    df_after = pl.read_csv(df_file)
    value_counts = df_after['split'].value_counts().to_dict(as_series=False)
    obtained = {k: v for k, v in zip(value_counts['split'], value_counts['counts'])}
    assert obtained == expected


@pytest.mark.parametrize('splits', [dict(a=0.5, b=0.4), dict(a=1.2, b=-0.2)])
def test_random_splitter_failure(splits):
    with pytest.raises(AssertionError):
        random_splitter('records_path', splits)
