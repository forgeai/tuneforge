import filecmp
import random
import string
from os.path import join

import librosa
import numpy as np
import polars as pl
import pytest

from core.data_pipeline.storage.tfrecords import (
    melspec2example,
    example2melspec,
    serialize_tfrecords,
)


def _generate_random_string(size: int = 10) -> str:
    return ''.join(random.choices(string.printable, k=size))


@pytest.mark.parametrize(
    'audio_path', [pytest.lazy_fixture('audio_wav'), pytest.lazy_fixture('audio_mp3')]
)
@pytest.mark.parametrize('sr', [22050, 44100])
@pytest.mark.parametrize('n_fft', [1024])
@pytest.mark.parametrize('hop_length', [256])
@pytest.mark.parametrize('n_mels', [32, 64])
def test_melspec2example_and_example2melspec(audio_path, sr, n_fft, hop_length, n_mels):
    y, sr = librosa.load(audio_path, sr=sr)
    original_M = librosa.feature.melspectrogram(
        y=y, sr=sr, n_fft=n_fft, hop_length=hop_length, n_mels=n_mels
    )
    original_name = _generate_random_string(10)

    example = melspec2example(
        audio_path,
        name=original_name,
        sr=sr,
        n_fft=n_fft,
        hop_length=hop_length,
        n_mels=n_mels,
    )
    record = example2melspec(example)
    retrieved_M = record['M'].numpy()
    retrieved_name = record['name'].numpy().decode('utf-8')

    assert retrieved_name == original_name
    assert np.array_equal(retrieved_M, original_M)


@pytest.mark.parametrize(
    'format, records_path',
    [
        ('.wav', pytest.lazy_fixture('wav_melspec_records_path')),
        ('.mp3', pytest.lazy_fixture('mp3_melspec_records_path')),
    ],
)
@pytest.mark.parametrize('sr', [44100])
def test_serialize_tfrecords(tmpdir, data_path, format, records_path, sr):
    serialize_tfrecords(data_path, tmpdir, format, sr=sr)

    comparison = filecmp.dircmp(tmpdir, records_path)
    assert len(comparison.common) == 5

    df = pl.read_csv(join(records_path, 'df.csv'))
    assert len(df) == 4
    assert df.columns == ['record', 'split']
