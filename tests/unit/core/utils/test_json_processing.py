from os.path import join, isfile

from core.utils.json_processing import (
    dict2json,
    dict2munch,
    json2dict,
    json2munch,
    Config,
    is_defined,
)


def test_all_json_processing_functions(tmpdir):
    test_dict = dict(a=1, b=2, c=dict(d=3))
    test_json_path = join(tmpdir, 'test.json')

    test_munch = dict2munch(test_dict)
    assert test_munch.a == 1
    assert test_munch.b == 2
    assert test_munch.c.d == 3

    dict2json(test_dict, test_json_path)
    assert isfile(test_json_path)

    test_dict_2 = json2dict(test_json_path)
    assert test_dict == test_dict_2

    test_munch_2 = json2munch(test_json_path)
    assert test_munch == test_munch_2


def test_config_creator():
    test_config = Config(a=1, b=2, c=dict(d=3))
    assert test_config.a == 1
    assert test_config.b == 2
    assert test_config.c.d == 3


def test_is_defined():
    test_config = Config(a=1, b=None, c={})
    assert is_defined(test_config, 'a')
    assert not is_defined(test_config, 'b')
    assert is_defined(test_config, 'c')
    assert not is_defined(test_config, 'e')
    assert not is_defined(test_config.c, 'e')
