import tensorflow as tf
from decouple import config as env_config


def test_current_tf_environment():
    jit_compile = env_config('TFG_JIT_COMPILE', default=False, cast=bool)
    jit_status = 'autoclustering' if jit_compile else ''
    assert tf.config.optimizer.get_jit() == jit_status

    run_eagerly = env_config('TFG_RUN_EAGERLY', default=False, cast=bool)
    assert tf.config.functions_run_eagerly() == run_eagerly

    gpu_memory_growth = env_config('TFG_GPU_MEMORY_GROWTH', default=True, cast=bool)
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        for gpu in gpus:
            assert tf.config.experimental.get_memory_growth(gpu) == gpu_memory_growth
