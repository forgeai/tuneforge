from argparse import Namespace
from os.path import basename, isfile, join, splitext

import pytest
from munch import Munch

from core.utils.json_processing import dict2json
from scripts.create_audiospecs import main as run_main


@pytest.fixture(scope='module')
def create_audiospecs_config():
    yield Munch.fromDict(
        {
            'fps': 2,
            'threads': 4,
            'processes': 4,
            'base_cmap': 'viridis',
            'highlight_cmap': 'afmhot',
            'highlight_sigma': 7.0,
            'melspec': {'sr': 11025, 'n_fft': 512, 'hop_length': 128, 'n_mels': 32},
        }
    )


@pytest.mark.parametrize(
    'audio_path', [pytest.lazy_fixture('audio_wav'), pytest.lazy_fixture('audio_mp3')]
)
@pytest.mark.parametrize('slice', [None, (3, 8)])
def test_create_audiospecs(tmpdir, audio_path, slice, create_audiospecs_config):
    if slice is not None:
        create_audiospecs_config.slice = slice

    config_file = join(str(tmpdir), 'config.json')
    dict2json(create_audiospecs_config, config_file)
    run_main(Namespace(config_file=config_file, inputs=audio_path, output=tmpdir))

    audio_name = splitext(basename(audio_path))[0]
    video_path = join(tmpdir, f'{audio_name}.mp4')

    assert isfile(video_path)
